package com.gym.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UMetrics implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "u_met_id", type = IdType.ID_WORKER)
    private Integer uMetId;

    private Double uMetWeight;

    private Double uMetBust;

    private Double uMetWaist;

    private Double uMetHipline;

    private Double uMetHeight;

    private Double uMetBmi;

    private Double uMetHeart;

    private Double uMetDheart;

    private Double uMetDthigh;

    private Double uMetXthigh;

    private Double uMetArm;

    private Double uMetFat;

    private Integer userId;


}
