package com.gym.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Curriculum implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "curr_id", type = IdType.ID_WORKER)
    private Integer currId;

    private String currName;

    private Integer userId;

    private Double currDuration;

    private Date currStart;

    private Integer typeDetaId;

    private Integer cDetaId;

    private Integer currState;

    private Date currEnd;

    private Integer placeId;

    private Double currMoney;
    @TableField(value = "user_name",exist = false)
    private String userName;
}
