package com.gym.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * (User)实体类
 *
 * @author makejava
 * @since 2020-07-09 08:27:04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class User implements Serializable {
    private static final long serialVersionUID = 448827620199024483L;
    @TableId(value = "user_id",type = IdType.AUTO)
    private Long userId;
    
    private String userName;
    
    private Integer userGender;
    
    private Integer userAge;

    private Date userTime;
    
    private String userPassword;
    
    private String userNumber;
    
    private Integer uTypeId;

    @TableField(value = "u_con_time" , exist = false)
    private Date uConTime;

    @TableField(value = "curr_id",exist = false)
    private Integer currId;

}