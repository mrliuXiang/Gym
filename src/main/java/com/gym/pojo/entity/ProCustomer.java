package com.gym.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ProCustomer implements Serializable {

    private static final long serialVersionUID = 1L;


    @TableId(value = "pro_id", type = IdType.AUTO)
    private Integer proId;


    private Long userId;


    private Date lastTime;


    private Integer proTalkedType;


    private Integer lastTalked;


    private String proResult;


}
