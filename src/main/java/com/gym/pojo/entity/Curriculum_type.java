package com.gym.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @ClassName Curriculum_type
 * @Description TODO
 * @Author Administrator
 * @Date 2020/7/23 15:39
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Curriculum_type implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "curriculum_type_id", type = IdType.ID_WORKER)
    private Long curriculumTypeId; //类型编号

    private String curriculumTypeName; //类型名字
}
