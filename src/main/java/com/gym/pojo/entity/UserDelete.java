package com.gym.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UserDelete implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "u_delete_id", type = IdType.ID_WORKER)
    private Integer uDeleteId;

    private String uDeleteName;

    private Integer uDeleteGender;

    private Integer uDeleteAge;

    private Date uDeleteTime;

    private String uDeletePassword;

    private String uDeleteTelephone;

    private Integer uDeleteCategory;


}
