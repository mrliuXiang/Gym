package com.gym.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class TypeDetailed implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "type_deta_id", type = IdType.ID_WORKER)
    private Integer typeDetaId;

    private String typeDetaName;


}
