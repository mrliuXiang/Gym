package com.gym.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class USurplus implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "u_surp_id", type = IdType.ID_WORKER)
    private Long uSurpId;

    private Long userId;

    private Date uSurpExpire;

    private Integer uSurpFrequency;

    private Date uSurpStart;

    @TableField(value = "user_name",exist = false)
    private String userName;

    @TableField(value = "user_gender" , exist = false)
    private Integer userGender;

}
