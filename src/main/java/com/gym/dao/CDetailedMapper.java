package com.gym.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gym.pojo.entity.CDetailed;
import com.gym.pojo.entity.Curriculum;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Mapper
public interface CDetailedMapper extends BaseMapper<CDetailed> {

    /**
     * 查看该课程有哪些学员
     * @return
     */
    @Select("SELECT d.* FROM `c_detailed` AS d INNER JOIN `curriculum` AS c ON c.`c_deta_id`=d.`c_deta_id`")
    IPage<CDetailed> selectCDetailed(IPage<CDetailed> page);
}
