package com.gym.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gym.pojo.entity.TypeDetailed;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Mapper
public interface TypeDetailedMapper extends BaseMapper<TypeDetailed> {

}
