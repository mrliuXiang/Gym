package com.gym.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gym.pojo.entity.Curriculum_type;
import com.gym.pojo.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface Curriculum_TypeMapper extends BaseMapper<Curriculum_type> {


}
