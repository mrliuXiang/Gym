package com.gym.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gym.pojo.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {


    @Select("select w.`curr_id`, u.`user_name`,u.`user_gender`,u.`user_age`,u.`user_number`,c.`u_con_time` from user as u \n" +
            "inner join `u_consumption` as c \n" +
            "on u.user_id=c.user_id\n" +
            "inner join `curriculum` as w\n" +
            "on w.user_id=c.user_id")
    IPage<User> selectUserPage(IPage<User> page);
}
