package com.gym.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gym.pojo.entity.ProCustomer;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-20
 */
public interface ProCustomerMapper extends BaseMapper<ProCustomer> {

}
