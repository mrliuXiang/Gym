package com.gym.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.gym.pojo.entity.USurplus;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Mapper
public interface USurplusMapper extends BaseMapper<USurplus> {

    /**
     *  查询会员信息
     * @param page 页面
     * @param queryWrapper 条件选择器
     * @return 分页信息
     */
    @Select("select s.* , u.user_name , u.user_gender from u_surplus as s inner join user as u on s.user_id=u.user_id ${ew.customSqlSegment}")
    IPage<USurplus> getSuperUser(IPage<USurplus> page,@Param(Constants.WRAPPER) QueryWrapper queryWrapper);


/*    *//**
     * 会员信息修改
     * @param queryWrapper 条件选择器
     * @return
     *//*
    @Update("update ")
    Boolean modifSuper(@Param(Constants.WRAPPER) QueryWrapper queryWrapper);*/

    /**
     * @return int
     * @Author 小飞
     * @描述 会员续费的方法
     * @Date 2020/7/14 14:40
     * @Param [id, Increasedtime] id是用户的主键id     Increasedtime 是用户想要添加的月份
     **/

    @Update("UPDATE u_surplus SET u_surp_expire=DATE_ADD(u_surp_expire,INTERVAL #{Increasedtime} MONTH) WHERE user_id=#{id}")
    Integer uprenew(@Param("id") int id, @Param("Increasedtime") int Increasedtime);




}
