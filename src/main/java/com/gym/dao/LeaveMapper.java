package com.gym.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.gym.pojo.entity.Leave;
import org.apache.ibatis.annotations.*;

import java.util.Date;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Mapper
public interface LeaveMapper extends BaseMapper<Leave> {


    /**
     * 条件查询请假列表
     * @param page 当前页
     * @param queryWrapper 条件选择
     * @return IPage<Leave>
     */
    @Select("SELECT l.*,u.`user_name` FROM `leave` AS l INNER JOIN `user` AS u ON u.`user_id`=l.`user_id` ${ew.customSqlSegment}")
    IPage<Leave> selectLeaveUser(IPage<Leave> page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

    /**
     * 新增请假员工
     * @param start 开始时间
     * @param end 结束时间
     * @param des 原由
     * @param state 处理状态
     * @param phone 电话
     * @return int
     */
    @Insert("INSERT INTO `leave` (`user_id`,`leave_start`,`leave_end`,`leave_remarks`,`leave_state`) VALUES" +
            " ((SELECT u.`user_id` FROM USER AS u WHERE u.`user_number`=#{phone}),#{start},#{end},#{des},#{state})")
    int insertLeaveUser(Date start , Date end , String des , Integer state, String phone);

    /**
     * 修改员工请假的信息
     * @param userid
     * @param leaveid
     * @param start
     * @param end
     * @param des
     * @param state
     * @param phone
     * @return
     */
    @Update("UPDATE `leave` SET `leave_start`=#{start} , `leave_end`=#{end}, `leave_remarks`=#{des} WHERE `leave_id`=#{leaveid}")
    int updaeLeaveUser(Integer userid , Integer leaveid,Date start , Date end , String des , Integer state, String phone);


    @Update("UPDATE `leave` SET `leave_state`=#{stateid} WHERE `leave_id`=#{leaveid}")
    int updateLeaveUser(Integer leaveid , Integer stateid);
}
