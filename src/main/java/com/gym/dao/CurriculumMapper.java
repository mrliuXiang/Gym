package com.gym.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gym.pojo.entity.Curriculum;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Mapper
public interface CurriculumMapper extends BaseMapper<Curriculum> {
    //私教的排课情况   id是教练的用户id
    /**
     * @Author 小飞
     * @描述
     * @Date 2020/7/13 9:18
     * @Param [page分页可写可不写, id是排课的教练的id]
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     **/
    @Select("SELECT c.*,a.type_deta_name FROM curriculum c INNER JOIN type_detailed a " +
            "ON c.type_deta_id=a.type_deta_id WHERE user_id=#{id}")
    List<Map<String,Object>> orderUserList(Page<Map<String,Object>> page, String id);

    /**
     * 查看课程信息
     * @param page
     * @param queryWrapper 条件选择器
     * @return IPage<Curriculum>
     */
    @Select("SELECT c.curr_id,c.curr_name, u.user_name ,c.curr_start , c.curr_end ," +
            " c.type_deta_id FROM `curriculum` AS c INNER JOIN `user` AS u " +
            "ON c.user_id=u.user_id ${ew.customSqlSegment}")
    IPage<Curriculum> selectCurriculumPage(IPage<Curriculum> page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);


}
