package com.gym.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gym.pojo.entity.CDetailed;
import com.gym.pojo.entity.ProCustomer;
import com.gym.service.CDetailedService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@RestController
@RequestMapping("/code/c-detailed")
public class CDetailedController {

    @Resource
    CDetailedService detailedService;


    /**
     *
     * @param currpage
     * @return IPage<CDetailed>
     */
    @GetMapping("/selectDetailed")
    public IPage<CDetailed> selectDetailed(Integer currpage){
       return detailedService.selectCDetailed(currpage);
    }
}

