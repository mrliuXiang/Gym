package com.gym.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gym.pojo.entity.Curriculum;
import com.gym.service.CurriculumService;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  课程前端控制器
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */

@Controller
@RequestMapping("/code/curriculum")
public class CurriculumController {
    @Resource
    CurriculumService curriculumService;


    /**
     * @Author 小飞
     * @描述  课程排列的方法
     * @Date 2020/7/14 15:23
     * @Param [id根据用户id来查询]
     * @return java.lang.String
     **/
    @RequestMapping("selectListPage")
    public  String   selectListPage(String id)
    {
        Page<Map<String, Object>> page = curriculumService.selectListPage(1, 2,id);
        List<Map<String,Object>> list=page.getRecords();

        return "page";
    }

    /**
     * 查看课程列表
     * @param currpage 当前页
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @param userName 用户名
     * @param cId 课程ID
     * @return IPage<Curriculum>
     */
    @GetMapping("/getCurriculumPage")
    public String getCurriculumPage(Model model, @RequestParam(name = "currpage" ,defaultValue = "1" ,required = false) int currpage,@RequestParam(defaultValue = "0001-00-00") String startTime ,@RequestParam(defaultValue = "0001-00-00") String endTime , String userName , Integer cId){
        //先将字符日期转为util日期
/*        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date dateStart =null;
        Date dateEnd =null;
        try {
            dateStart=df.parse(startTime);
            dateEnd=df.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //由于util在sql中不符，再转为sql日期
        java.sql.Date dayDateSqlStart = new java.sql.Date(dateStart.getTime());
        java.sql.Date dayDateSqlEnd = new java.sql.Date(dateEnd.getTime());*/
        IPage<Curriculum> curipg=curriculumService.getCurriculumPage(currpage);
        model.addAttribute("curipg",curipg);
        return "kecheng";
    }

    /**
     * 按条件查看课程列表  吴业煜 2020年7月22日22:12:50
     * @param currpage 当前页
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @param userName 用户名
     * @param cId 课程ID
     * @return IPage<Curriculum>
     */
    @ResponseBody
    @GetMapping("/getCurriculumPageByConditions")
    public Object getCurriculumPageByConditions(Model model, @RequestParam(name = "currpage" ,defaultValue = "1" ,required = false) int currpage, String startTime , String endTime , String userName , Integer cId){

        //先将字符日期转为util日期
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date dateStart =null;
        Date dateEnd =null;

        java.sql.Date dayDateSqlStart=null;
        java.sql.Date dayDateSqlEnd=null;
        if(!startTime.equals("")){
            try {
                dateStart=df.parse(startTime);
                dayDateSqlStart = new java.sql.Date(dateStart.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if(!endTime.equals("")){
            try {
                dateEnd=df.parse(endTime);
                dayDateSqlEnd = new java.sql.Date(dateEnd.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //由于util在sql中不符，再转为sql日期
        IPage<Curriculum> curipg=curriculumService.getCurriculumPage(currpage, dayDateSqlStart, dayDateSqlEnd, userName, cId);
        return curipg;
    }

    /**
     * 添加课程
     * @param curriculum
     * @return string
     */

    @PostMapping("/addCurriculum")
    public String addCurriculum(Curriculum curriculum){
        return curriculumService.addCurriculum(curriculum)>0?"true":"false";
    }

    /**
     * 删除课程
     * @param currId 课程id
     * @return String
     */
    @ResponseBody
    @PostMapping("/delCurriculum")
    public String delCurriculum(Integer currId){
       return curriculumService.delCurriculum(currId)>0?"true":"false";
    }

    /**
     * 修改课程
     * @param curriculum
     * @return String
     */
    @PostMapping("/modifyCurriculum")
    public String modifyCurriculum(Curriculum curriculum){
       return curriculumService.modifyCurriculum(curriculum)>0?"true":"false";
    }
}

