package com.gym.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gym.pojo.entity.Leave;
import com.gym.service.LeaveService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@RestController
@RequestMapping("/code/leave")
public class LeaveController {
    @Resource
    LeaveService leaveService;

    /**
     * 获取请假用户列表
     * @param currpage
     * @param leavestart
     * @param leaveend
     * @param userid
     * @param leavestate
     * @param username
     * @return IPage<Leave>
     */
    @GetMapping("/getLeaveUser")
    public IPage<Leave> getLeaveUser(Integer currpage, Date leavestart , Date leaveend , Integer userid , Integer leavestate , String username){

        return leaveService.selectLeave(currpage, leavestart, leaveend, userid, leavestate, username);
    }

    /**
     * 新增请假用户
     * @param start 开始时间
     * @param end 结束时间
     * @param des 原由
     * @param state 处理状态
     * @param phone 电话
     * @return String
     *
     */
    @PostMapping("/addLeaveUser")
    public String addLeaveUser(Date start , Date end , String des , @RequestParam(name = "state" ,defaultValue = "0" ,required = false) Integer state, String phone){
        return leaveService.addLeaveUser(start,end,des,state,phone)>0?"true":"false";
    }

    /**
     * 修改员工请假信息
     * @param userid 员工id
     * @param leaveid 请假id
     * @param start 请假开始时间
     * @param end 请假结束时间
     * @param des 请假原由
     * @param state 处理的状态
     * @param phone 电话
     * @return String
     */
    @PostMapping("/updateLeaveUser")
    public String updateLeaveUser(Integer userid , Integer leaveid,Date start , Date end , String des , Integer state, String phone){
        return  leaveService.updateLeaveUser(userid, leaveid, start, end, des, state, phone)>0?"true":"false";
    }

    /**
     * 审批请假
     * @param leaveid 请假id
     * @param stateid 审批结果id
     * @return String
     *
     */
    @PostMapping("/updateLeaveState")
    public String updateLeaveState(Integer leaveid , Integer stateid){
        return leaveService.updateLeaveState(leaveid,stateid)>0?"true":"fasle";
    }

}

