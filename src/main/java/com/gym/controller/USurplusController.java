package com.gym.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gym.pojo.entity.USurplus;
import com.gym.service.USurplusService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@RestController
@RequestMapping("/code/u-surplus")
public class USurplusController {

    @Resource
    USurplusService surplusService;

    /**
     * 查询会员信息
     * @param start 开始时间
     * @param expire 结束时间
     * @param gender 性别
     * @param username 用户名
     * @param userid 用户编号
     * @return 分页返回会员信息
     */
    @GetMapping("/getSuperUser")
    public IPage<USurplus> getSuperUser(String start , String expire , Long gender , String username , String userid){
       return surplusService.getSuperUser(start,expire,gender,username,userid);
    }

    @GetMapping("/getuphuiyuan")
    public String getuphy(int id,int Increasedtime) {

        if (surplusService.uprenew(id,Increasedtime)>0){
            return "成功";
        }else {
            return "失败";
        }

    }
}

