package com.gym.controller;


import com.gym.pojo.entity.Equipment;
import com.gym.service.EquipmentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Controller
@RequestMapping("/code/equipment")
public class EquipmentController {


    @Resource
    EquipmentService equipmentService;

    @RequestMapping("/addindex")
    public String index(Model model){
        List<Equipment> list=equipmentService.seleequ();
        model.addAttribute("equimp",list);
        return "cate";
    }

    @RequestMapping("/addequ")
    @ResponseBody
    public String addequ(Equipment equipment){
       if (equipmentService.addequipm(equipment)>0){
           return "true";
       }else {
           return "false";
       }

    }

    @RequestMapping("/upequ")
    @ResponseBody
    public String updeequ(Equipment equipment){
        if (equipmentService.upequipm(equipment)>0){
            return "true";
        }else{
            return "false";
        }
    }

    @RequestMapping("/deleequ")
    @ResponseBody
    public String deleequ(int equidd){
        if (equipmentService.deleequipm(equidd)>0){
            return "true";
        }else{
                return "false";
        }
    }
}

