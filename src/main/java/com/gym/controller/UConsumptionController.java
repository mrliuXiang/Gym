package com.gym.controller;


import com.gym.pojo.entity.UConsumption;
import com.gym.service.UConsumptionService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.sql.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 * 消费
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@RestController
@RequestMapping("/code/u-consumption")
public class UConsumptionController {

    @Resource
    UConsumptionService uConsumptionService;

    @RequestMapping("/xiaofei")
    public String xiaofei(@RequestParam(name = "uconmoney" ,defaultValue = "1" ,required = false) double uconmoney, Date ucontime){
        List<UConsumption> uclist=uConsumptionService.getConsum(uconmoney, ucontime);
        for (UConsumption uConsumption:uclist){
            System.out.println(uConsumption.getUConRemarks());
        }
       /* UConsumption uConsumption=new UConsumption();
        uConsumption.setUserId(1);
        uConsumption.setUConMoney(0.1);
        uConsumption.setUConTime(ucontime);
        uConsumption.setUConRemarks("dadada");
        uConsumption.setUConType(0);
        uConsumptionService.addUcon(uConsumption);*/


        return "aa";
    }

    /**
     * 用户购买课程消费
     * 首先需要获取课程的id等信息
     * @param uConsumption
     * @return String
     */
    @PostMapping("/addUConsumption")
    public String addUConsumption(UConsumption uConsumption){
       return uConsumptionService.addUConsumption(uConsumption)>0?"true":"false";
    }

}

