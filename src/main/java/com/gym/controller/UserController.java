package com.gym.controller;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gym.pojo.entity.User;
import com.gym.service.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Controller
@RequestMapping("/code/user")
public class UserController {

    @Resource
    UserService userService;

    /**
     * 测试跳转
     */
    @RequestMapping("/index")
    public String index() {
        return "index";
    }

    @RequestMapping("/chaxun")
    @ResponseBody
    public IPage<User> getcx(@RequestParam(name = "currpage" ,defaultValue = "1" ,required = false)int currpage,String username, String phone, Integer sex,Model model) {
        System.out.println("用户名字"+sex);
        IPage<User> list = userService.getUserByid(currpage,username,phone,sex);
        model.addAttribute("users", list);
        return list;
    }


    /**
     * 测试跳转
     */
    @RequestMapping("/index1")
    public String userList(@RequestParam(name = "currpage" ,defaultValue = "1" ,required = false)int currpage,Model model) {
        IPage<User> list = userService.getUSerList(currpage);

        model.addAttribute("users", list);

        return "member-list";
    }


    @RequestMapping("/member-edit1")
    public ModelAndView getUser(@RequestParam("userid") Integer id) {
        ModelAndView model = new ModelAndView();
        User user = userService.getUserByid(id);
        model.addObject("user", user);
        model.setViewName("member-edit");
        return model;
    }
    @RequestMapping("member-password")
    public String getupuser(@RequestParam(name = "userId",required = false) Integer id,Model model){
        model.addAttribute("userId",id);
        return "member-password";
    }

    //更新密码的方法
    @RequestMapping("member-password1")
    @ResponseBody
    public String getupuser2( Integer userId,String oldPass,String newPass,String rePass,Model model){
        model.addAttribute("userId",userId);
            if (newPass.equals(rePass)){

                if (userService.modifyUserPwd(userId.toString(),oldPass,newPass)){

                    return "true";
                }else {

                    return "false";
                }
            }else {

                return "false";
            }



    }


    /**
     * 用户登陆
     *
     * @param username 用户名
     * @param password 用户密码
     * @return user
     */
    @PostMapping("/login")
    public User userLogin(String username, String password) {
        //或者前端判断user是否为空;对应输出信息
        User user = userService.userLogin(username, password);
        if (user != null) {
            System.out.println("登录成功");
            return user;
        } else {
            return user;
        }
    }

    /**
     * 用户信息修改
     *
     * @param userId   用户ID
     * @param username 用户名
     * @param phone    电话
     * @param sex      性别
     * @param age      年龄
     * @return Boolean
     */
    @PostMapping("/modifyInfo")
    @ResponseBody
    public String modifyUser(Integer userId, String username, String phone,Long sex, Long age) {
        System.out.println("con"+userId);
        if (userService.modifyUser(userId.toString(), phone, username, sex, age)){
            return"true";
        }else {
            return "false";
        }

    }

    /**
     * 用户密码修改
     *
     * @param userid   用户id
     * @param password 用户密码(旧密码)
     * @param newpwd   用户新密码
     * @param repass   确认密码
     * @return String
     */
    @PostMapping("/update/pwd")
    public String updatePwd(String userid, String password, String newpwd, String repass) {
        if (newpwd == repass) {
            return "false";
        }
        userService.modifyUserPwd(userid, password, newpwd);
        return "true";
    }

    /**
     * 用户删除
     *
     * @param userid
     * @return String
     */
    @GetMapping("/delete")
    @ResponseBody
    public String delUser(String userid) {
        Boolean delUser = userService.delUser(userid);
        if (delUser != true) {
            return "false";
        }
        return "true";
    }

    @RequestMapping("/member-add")
    public String asd(){
        return "member-add";
    }

    @PostMapping("/member-add1")
    @ResponseBody
    public String dad1(User user){
        if (userService.insertUser(user)>0){
            System.out.println("成功");
            return "true";
        }else {
            return "false";
        }

    }

    @RequestMapping("/erweima")
    public String erw(){
        return "erweima";
    }

    @RequestMapping("/admin-charge")
    public String showchar(){
        return "admin-charge";
    }

    @GetMapping("/getPageUser")
    public IPage<User> getIPageUser(String start, String expire, Long gender, String username, String userid) {
        return userService.getPageUser(start, expire, gender, username, userid);
    }
}

