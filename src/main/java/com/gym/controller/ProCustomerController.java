package com.gym.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gym.pojo.entity.ProCustomer;
import com.gym.service.ProCustomerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-20
 */
@RestController
@RequestMapping("/code/pro-customer")
public class ProCustomerController {

    @Resource
    ProCustomerService proCustomerService;

    /**
     * 分页显示
     * 获取潜在客户的列表
     *
     * @return List<ProCustomer>
     */
    @GetMapping("/getproCustomer")
    public IPage<ProCustomer> getproCustomer() {
        return proCustomerService.selectProCustomer();
    }

    /**
     * 修改潜在客户的信息
     *
     * @param proCustomer
     * @return String
     */
    @PostMapping("/updateProCustomer")
    public String updateProCustomer(ProCustomer proCustomer){
      return proCustomerService.updaProCustomer(proCustomer)?"true":"false";
    }
}

