package com.gym.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gym.pojo.entity.Equipment;

import java.util.List;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
public interface EquipmentService extends IService<Equipment> {

    int addequipm(Equipment equipment);

    int upequipm(Equipment equipment);

    int deleequipm(int equidd);

    List<Equipment> seleequ();
}
