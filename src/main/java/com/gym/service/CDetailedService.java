package com.gym.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gym.pojo.entity.CDetailed;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
public interface CDetailedService extends IService<CDetailed> {

    /**
     * 获取课程详情列表
     * @param currpage 当前页码
     * @return IPage<CDetailed>
     */
    IPage<CDetailed> selectCDetailed(Integer currpage);


    /**
     * 给该课程增加学员
     * @param detailed
     * @return
     */
    int addCDetailedByUser(CDetailed detailed);


}
