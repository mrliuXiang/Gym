package com.gym.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.gym.dao.PlaceMapper;
import com.gym.pojo.entity.Place;
import com.gym.service.PlaceService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Service
public class PlaceServiceImpl extends ServiceImpl<PlaceMapper, Place> implements PlaceService {

}
