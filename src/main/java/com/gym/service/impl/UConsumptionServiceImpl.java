package com.gym.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.gym.dao.UConsumptionMapper;
import com.gym.pojo.entity.ProCustomer;
import com.gym.pojo.entity.UConsumption;
import com.gym.service.ProCustomerService;
import com.gym.service.UConsumptionService;
import org.apache.tomcat.jni.Proc;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Service
public class UConsumptionServiceImpl extends ServiceImpl<UConsumptionMapper, UConsumption> implements UConsumptionService {
    @Resource
    UConsumptionMapper uConsumptionMapper;
    @Resource
    ProCustomerService proCustomerService;
    @Override
    public List<UConsumption> getConsum(double uconmoney, Date ucontime) {
        QueryWrapper<UConsumption> condition=new QueryWrapper<UConsumption>();
        if (ucontime!=null){
            condition.eq("u_con_time",ucontime);
        }else{
            condition.eq("u_con_money",uconmoney);
        }


        return uConsumptionMapper.selectList(condition);
    }

    @Override
    public int addUcon(UConsumption uConsumption) {

        return uConsumptionMapper.insert(uConsumption);
    }

    /**
     * 0消费
     */
    @Override
    public void selectNoMoney() {
        QueryWrapper<UConsumption> condition = new QueryWrapper<>();
        condition.eq("u_con_money",0);
        ProCustomer proCustomer=null;
        List<UConsumption> consumptionList = uConsumptionMapper.selectList(condition);
        for (UConsumption consumer : consumptionList){
            proCustomer = new ProCustomer();
            proCustomer.setUserId(consumer.getUserId());
            ProCustomer proCustomer1 = proCustomerService.selectProCustomerOne(consumer.getUserId());
            if (proCustomer1!=null){
                return;
            }
            proCustomerService.saveOrUpdate(proCustomer);
        }
    }

    /**
     * 消费购买课程
     * @param uConsumption
     * @return int
     */
    @Override
    public int addUConsumption(UConsumption uConsumption) {

        return uConsumptionMapper.insert(uConsumption);
    }


}
