package com.gym.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.gym.dao.CurriculumMapper;
import com.gym.pojo.entity.Curriculum;
import com.gym.service.CurriculumService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Date;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Service
public class CurriculumServiceImpl extends ServiceImpl<CurriculumMapper, Curriculum> implements CurriculumService {

    @Resource
    CurriculumMapper curriculumMapper;
    /**
     * @Author 小飞
     * @描述  这是查询排课的方法
     * @Date 2020/7/14 13:22
     * @Param [current, number, id]  前两个是分页，就是教练的id
     * @return com.baomidou.mybatisplus.extension.plugins.pagination.Page<java.util.Map<java.lang.String,java.lang.Object>>
     **/
    @Override
    public Page<Map<String, Object>> selectListPage(int current, int number, String id) {
        Page<Map<String,Object>> page =new Page<Map<String,Object>>(current,number);
        return page.setRecords(this.baseMapper.orderUserList(page,id));
    }

    /**
     * 分页查看课程信息
     * @param currpage 当前页
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @param userName 用户名
     * @param cId 课程id
     * @return
     */
    @Override
    public IPage<Curriculum> getCurriculumPage(int currpage,Date startTime , Date endTime , String userName , Integer cId) {

        QueryWrapper<Curriculum> condition = new QueryWrapper<>();
        condition.ge(startTime!=null,"c.curr_start",startTime);  //ge 大于或等于
        condition.le(endTime!=null,"c.curr_end",endTime); //le 小于或等于
        condition.like(userName!=null&&!userName.equals(""),"u.user_name",userName);
        condition.eq(cId!=null&&cId!=0,"c.type_deta_id",cId);
        IPage<Curriculum> page = new Page<>(currpage,5);
        IPage<Curriculum> selectPage = curriculumMapper.selectCurriculumPage(page,condition);
        return selectPage;
    }

    @Override
    public IPage<Curriculum> getCurriculumPage(int currpage) {
        IPage<Curriculum> page = new Page<>(currpage,5);
        return curriculumMapper.selectPage(page,null);
    }

    /**
     * 添加课程
     * @param curriculum
     * @return int
     */
    @Override
    public int addCurriculum(Curriculum curriculum) {
        return curriculumMapper.insert(curriculum);
    }

    /**
     * 删除课程
     * @param currId 课程ID
     * @return int
     */
    @Override
    public int delCurriculum(Integer currId) {
        return curriculumMapper.deleteById(currId);
    }

    /**
     * 修改信息
     * @param curriculum
     * @return int
     */
    @Override
    public int modifyCurriculum(Curriculum curriculum) {
        QueryWrapper<Curriculum> condition = new QueryWrapper<>();
        condition.eq("curr_id",curriculum.getCurrId());
        Curriculum curriculum1 = curriculumMapper.selectOne(condition);
        UpdateWrapper<Curriculum> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set(curriculum.getCurrName()!=null,"curr_name",curriculum.getCurrName());
        updateWrapper.set(curriculum.getCurrStart()!=null,"curr_start",curriculum.getCurrStart());
        updateWrapper.set(curriculum.getCurrEnd()!=null,"curr_end",curriculum.getCurrEnd());
        updateWrapper.set(curriculum.getCurrMoney()!=null,"curr_money",curriculum.getCurrMoney());
        updateWrapper.set(curriculum.getTypeDetaId()!=null,"type_deta_id",curriculum.getTypeDetaId());
        updateWrapper.set(curriculum.getUserId()!=null,"user_id",curriculum.getUserId());
        return curriculumMapper.update(curriculum1,updateWrapper);
    }
}
