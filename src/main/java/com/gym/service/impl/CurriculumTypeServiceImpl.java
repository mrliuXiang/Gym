package com.gym.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gym.dao.Curriculum_TypeMapper;
import com.gym.pojo.entity.Curriculum_type;
import com.gym.service.CurriculumTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName CurriculumTypeServiceImpl
 * @Description TODO
 * @Author Administrator
 * @Date 2020/7/23 15:52
 **/
@Service
public class CurriculumTypeServiceImpl extends ServiceImpl<Curriculum_TypeMapper,Curriculum_type> implements CurriculumTypeService {

    @Resource
    Curriculum_TypeMapper curriculum_typeMapper;

    /**
     * 获取健身类型集合
     * @return List<Curriculum_type>
     */
    @Override
    public List<Curriculum_type> getCurriculumTypeName() {
        List<Curriculum_type> list=  curriculum_typeMapper.selectList(null);
        return list;
    }


}
