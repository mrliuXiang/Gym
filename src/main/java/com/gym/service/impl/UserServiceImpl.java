package com.gym.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.gym.dao.UserMapper;
import com.gym.pojo.entity.Curriculum;
import com.gym.pojo.entity.User;
import com.gym.service.UserService;
import com.sun.xml.internal.bind.v2.model.core.EnumLeafInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {


    /**
     * 分页
     * page_curr 当前页码
     * page_size 每页显示数据
     */
    private static Long page_curr = 0L;
    private static Long page_size = 12L;
    @Resource
    UserMapper userMapper;


    @Override
    public User getUserByid(Integer id) {
        return userMapper.selectById(id);
    }

    @Override
    public IPage<User> getUserByid(int currpage,String username, String phone, Integer sex) {
        QueryWrapper<User> condition1 = new QueryWrapper<>();

        condition1.eq("user_name", username.trim()).or().eq("user_number", phone.trim()).or().eq("user_gender", sex);

        IPage<User> page = new Page<>(currpage,3);
        IPage<User> selectPage = userMapper.selectPage(page,condition1);

        return selectPage;
    }

    @Override
    public IPage<User> getUSerList(int currpage) {
        QueryWrapper<User> condition = new QueryWrapper<>();
        IPage<User> page = new Page<>(currpage,3);
        IPage<User> selectPage = userMapper.selectPage(page,condition);
        return selectPage;
    }

    /**
     * 用户登陆
     *
     * @param username 用户名
     * @param password 密码
     * @return
     */
    @Override
    public User userLogin(String username, String password) {
        QueryWrapper<User> condition = new QueryWrapper<>();
        if (username != null && password != null) {
            condition.eq("user_name", username);
            condition.eq("user_password", password);
        } else {
            condition.eq("user_name", username);
        }
        return userMapper.selectOne(condition);
    }

    /**
     * 修改用户信息
     *
     * @param userid   用户id
     * @param phone    电话
     * @param username 用户名
     * @param sex      性别
     * @param age      年龄
     * @return
     */
    @Override
    public Boolean modifyUser(String userid, String phone, String username, Long sex, Long age) {
        UpdateWrapper<User> condition = new UpdateWrapper<>();
        System.out.println("impl" + userid);
        condition.set("user_number", phone);
        condition.set("user_name", username);
        condition.set("user_gender", sex);
        condition.set("user_age", age);
        condition.eq("user_id", userid);

        User user = userMapper.selectById(userid);
        System.out.println(user);
        int update = userMapper.update(user, condition);
        if (update > 0) {
            return true;
        }
        return false;
    }

    /**
     * 用户修改密码
     *
     * @param userid   用户id
     * @param password 用户密码(旧密码)
     * @param newPwd   用户新密码
     * @return boolean
     */
    @Override
    public Boolean modifyUserPwd(String userid, String password, String newPwd) {
        User user = userMapper.selectById(userid);
        //System.out.println(user);
        UpdateWrapper<User> condition = new UpdateWrapper<>();
        condition.set("user_password", newPwd);
        condition.eq("user_id", userid);
        if (!user.getUserPassword().equals(password)) {
            return false;
        }
        userMapper.update(user, condition);
        return true;
    }

    /**
     * 用户删除
     *
     * @param userid 用户删除
     * @return boolean
     */
    @Override
    public Boolean delUser(String userid) {
        int byId = userMapper.deleteById(userid);
        if (byId > 0) {
            return true;
        }
        return false;
    }

    /**
     * 新增用户
     *
     *
     * @return int
     */
    @Override
    public int insertUser(User user) {

        user.setUserTime(new Date());
        user.setUTypeId(1);
        return userMapper.insert(user);
    }

    /**
     * 查询分页查询
     *
     * @param start
     * @param expire
     * @param gender
     * @param username
     * @param userid
     * @return IPage
     */
    @Override
    public IPage<User> getPageUser(String start, String expire, Long gender, String username, String userid) {
        QueryWrapper<User> condition = new QueryWrapper<>();
       /* 最好要引入:<!--  <dependency>
                    <groupId>org.apache.commons</groupId>
                    <artifactId>commons-lang3</artifactId>
                    <version>3.9</version>
                </dependency>-->
                使用StringUtils来判断是否进行sql拼接
                */
        condition.like(userid != null, "user_id", userid);
        condition.like(gender != null, "user_gender", gender);
        condition.like(username != null, "user_name", username);
        if (expire != null) {
            condition.between(expire != null, "user_time", start, expire);
        } else {
            condition.like(start != null, "user_time", start);
        }
        IPage<User> page = new Page<>(page_curr, page_size);
        IPage<User> pageUser = userMapper.selectPage(page, condition);
        return pageUser;

    }

    @Override
    public IPage<User> getPageByCon(Integer currpage) {
        return null;
    }


}
