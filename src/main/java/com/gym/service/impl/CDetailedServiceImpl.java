package com.gym.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.gym.dao.CDetailedMapper;
import com.gym.pojo.entity.CDetailed;
import com.gym.pojo.entity.ProCustomer;
import com.gym.service.CDetailedService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Service
public class CDetailedServiceImpl extends ServiceImpl<CDetailedMapper, CDetailed> implements CDetailedService {

    @Resource
    CDetailedMapper cDetailedMapper;

    /**
     *
     * @param currpage
     * @return IPage<CDetailed>
     */
    @Override
    public IPage<CDetailed> selectCDetailed(Integer currpage) {
        IPage<CDetailed> page = new Page<>(currpage,12);

        return cDetailedMapper.selectCDetailed(page);
    }

    @Override
    public int addCDetailedByUser(CDetailed detailed) {
        return 0;
    }
}
