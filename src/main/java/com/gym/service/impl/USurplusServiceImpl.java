package com.gym.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.gym.dao.USurplusMapper;
import com.gym.pojo.entity.ProCustomer;
import com.gym.pojo.entity.UConsumption;
import com.gym.pojo.entity.USurplus;
import com.gym.service.ProCustomerService;
import com.gym.service.USurplusService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Date;
import java.text.SimpleDateFormat;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Service
public class USurplusServiceImpl extends ServiceImpl<USurplusMapper, USurplus> implements USurplusService {

    @Resource
    ProCustomerService proCustomerService;
    /**
     * 分页
     * page_curr 当前页码
     * page_size 每页显示数据
     */
    private static Long page_curr=0L;
    private static Long page_size=12L;

    @Resource
    USurplusMapper surplusMapper;

    /**
     *
     * @param start 开始时间
     * @param expire 结束时间
     * @param gender 性别
     * @param username 用户名
     * @param userid 用户id
     * @return  IPage<USurplus>
     */
    @Override
    public IPage<USurplus> getSuperUser(String start, String expire, Long gender, String username, String userid) {
        QueryWrapper<USurplus> condition = new QueryWrapper<>();
       /* 最好要引入:<!--  <dependency>
                    <groupId>org.apache.commons</groupId>
                    <artifactId>commons-lang3</artifactId>
                    <version>3.9</version>
                </dependency>-->
                使用StringUtils来判断是否进行sql拼接
                */
        condition.like(userid!=null,"u.user_id",userid);
        condition.like(expire!=null,"s.u_surp_expire",expire);
        condition.like(gender!=null,"u.user_gender",gender);
        condition.like(username!=null,"u.user_name",username);
        condition.like(start!=null,"s.u_surp_start",start);
        IPage<USurplus> page = new Page<>(page_curr , page_size);
        IPage<USurplus> superUser = surplusMapper.getSuperUser(page,condition);
        return superUser;

    }

    @Override
    public Integer uprenew(int id, int Increasedtime) {
        return surplusMapper.uprenew(id, Increasedtime);
    }

    /**
     * 过期的用户
     */
    @Override
    public void overDueUser() {
        QueryWrapper<USurplus> condition = new QueryWrapper<>();
        //获取当前时间
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        condition.le("u_surp_expire",date);
        condition.eq("u_surp_frequency",0);
        ProCustomer proCustomer =null;
        List<USurplus> surpluses = surplusMapper.selectList(condition);
        for (USurplus surplus : surpluses){
            proCustomer = new ProCustomer();
            proCustomer.setUserId(surplus.getUserId());
            ProCustomer proCustomer1 = proCustomerService.selectProCustomerOne(surplus.getUserId());
            if (proCustomer1!=null){
                return;
            }
            proCustomerService.saveOrUpdate(proCustomer);
        }

    }
}
