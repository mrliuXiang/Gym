package com.gym.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.gym.dao.ProCustomerMapper;
import com.gym.pojo.entity.ProCustomer;
import com.gym.service.ProCustomerService;
import com.gym.service.UConsumptionService;
import com.gym.service.USurplusService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-20
 */
@Service
public class ProCustomerServiceImpl extends ServiceImpl<ProCustomerMapper, ProCustomer> implements ProCustomerService {
    /**
     * 分页
     * page_curr 当前页码 (下一页的话则需要进行前端传值)
     * page_size 每页显示数据
     */
    private static Long page_curr=0L;
    private static Long page_size=12L;
    //获取打卡次数大于20次/月的用户(还没有添加)
    @Resource
    USurplusService surplusService;
    //获取没有消费信息的用户
    @Resource
    UConsumptionService consumptionService;
    //卡过期的用户
    @Resource
    ProCustomerMapper customerMapper;
    @Override
    public IPage<ProCustomer> selectProCustomer() {
        consumptionService.selectNoMoney();
        surplusService.overDueUser();
        IPage<ProCustomer> page = new Page<>(page_curr , page_size);
        //可以添加条件
        QueryWrapper<ProCustomer> condition = new QueryWrapper<>();
        IPage<ProCustomer> superUser = customerMapper.selectPage(page,condition);

        return superUser;

    }

    @Override
    public int insert(ProCustomer proCustomer) {
        customerMapper.insert(proCustomer);
        return 1;
    }

    @Override
    public int modifyProCustomer(ProCustomer proCustomer) {
        QueryWrapper<ProCustomer> con = new QueryWrapper<>();
        con.eq("user_id",proCustomer.getUserId());
        ProCustomer customer = customerMapper.selectOne(con);
        UpdateWrapper<ProCustomer> condition = new UpdateWrapper<>();
        condition.set(proCustomer.getProResult()!=null,"pro_result",proCustomer.getProResult());
        condition.set(proCustomer.getLastTalked()!=null,"last_talked",proCustomer.getLastTalked());
        condition.set(proCustomer.getLastTime()!=null,"last_time",proCustomer.getLastTime());
        condition.set(proCustomer.getProTalkedType()!=null,"pro_talked_type",proCustomer.getProTalkedType());
        return customerMapper.update(customer,condition);
    }

    @Override
    public ProCustomer selectProCustomerOne(Long userid) {
        QueryWrapper<ProCustomer> condition = new QueryWrapper<>();
        condition.eq(userid!=null,"user_id",userid);
        return customerMapper.selectOne(condition);
    }

    @Override
    public Boolean updaProCustomer(ProCustomer proCustomer) {
        QueryWrapper<ProCustomer> condition = new QueryWrapper<>();
        condition.eq(proCustomer.getUserId()!=null,"user_id",proCustomer.getUserId());
        ProCustomer customer = customerMapper.selectOne(condition);
        UpdateWrapper<ProCustomer> upda = new UpdateWrapper<>();
        upda.set(proCustomer.getProResult()!=null,"pro_result",proCustomer.getProResult());
        upda.set(proCustomer.getLastTalked()!=null,"last_talked",proCustomer.getLastTalked());
        upda.set(proCustomer.getLastTime()!=null,"last_time",proCustomer.getLastTime());
        upda.set(proCustomer.getProTalkedType()!=null,"pro_talked_type",proCustomer.getProTalkedType());
        upda.eq(proCustomer.getUserId()!=null,"user_id",proCustomer.getUserId());
        return customerMapper.update(customer,upda)>0?true:false;
    }
}
