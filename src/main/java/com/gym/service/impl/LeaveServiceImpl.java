package com.gym.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.gym.dao.LeaveMapper;
import com.gym.dao.UserMapper;
import com.gym.pojo.entity.Leave;
import com.gym.pojo.entity.User;
import com.gym.service.LeaveService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Service
public class LeaveServiceImpl extends ServiceImpl<LeaveMapper, Leave> implements LeaveService {

    @Resource
    LeaveMapper leaveMapper;
    @Resource
    UserMapper userMapper;
    @Override
    public IPage<Leave> selectLeave(Integer currpage, Date leavestart , Date leaveend , Integer userid , Integer leavestate , String username) {
        QueryWrapper<Leave> condition = new QueryWrapper<>();
        condition.like(username!=null,"u.user_name",username);
        condition.like(userid!=null,"l.user_id",userid);
        condition.like(leavestart!=null,"l.leave_start",leavestart);
        condition.like(leaveend!=null,"l.leave_end",leaveend);
        condition.eq(leavestate!=null,"l.leave_state",leavestate);
        IPage<Leave> page = new Page<>(currpage,12);
        return leaveMapper.selectLeaveUser(page,condition);
    }

    @Override
    public int addLeaveUser(Date start , Date end , String des , Integer state, String phone) {
        QueryWrapper<User> condition = new QueryWrapper<>();
        condition.eq("user_number",phone);
        User user = userMapper.selectOne(condition);
       /* Leave leave = new Leave();
        leave.setUserId(user.getUserId())
                .setLeaveStart(start)
                .setLeaveEnd(end)
                .setLeaveState(state)
                .setLeaveRemarks(des);*/
        return leaveMapper.insertLeaveUser(start,end,des,state,phone);
    }

    /**
     * 修改请假员工信息
     * @param
     * @param phone
     * @return int
     */
   /* @Override
    public int updateLeaveUser(Leave leave , String phone) {
        QueryWrapper<User> condi = new QueryWrapper<>();
        condi.eq("user_id",leave.getUserId());
        User user = userMapper.selectOne(condi);
        UpdateWrapper<Leave> condition = new UpdateWrapper<>();
        condition.set("leave_start",leave.getLeaveStart());
        condition.set("leave_end",leave.getLeaveEnd());
        condition.set("leave_remarks",leave.getLeaveRemarks());
        condition.eq("leave_id",leave.getLeaveId());
        user.setUserNumber(phone);
        userMapper.updateById(user);
        return leaveMapper.update(leave,condition);
    }*/
    @Override
    public int updateLeaveUser(Integer userid , Integer leaveid, Date start , Date end , String des , Integer state, String phone) {
        QueryWrapper<User> condi = new QueryWrapper<>();
        condi.eq("user_id",userid);
        User user = userMapper.selectOne(condi);
        UpdateWrapper<Leave> condition = new UpdateWrapper<>();
        condition.set("leave_start",start);
        condition.set("leave_end",end);
        condition.set("leave_remarks",des);
        condition.eq("leave_id",leaveid);
        user.setUserNumber(phone);
        userMapper.updateById(user);
        return leaveMapper.updaeLeaveUser(userid,leaveid,start,end,des,state,phone);
    }

    /**
     * 审批请假
     * @param leaveId 请假id
     * @param stateid 审批结果
     * @return int
     */
    @Override
    public int updateLeaveState(Integer leaveId , Integer stateid) {
       /* //QueryWrapper<Leave> con = new QueryWrapper<>();
        Leave leave1 = leaveMapper.selectById(leaveId);
        System.out.println(leave1);
        UpdateWrapper<Leave> contition = new UpdateWrapper<>();
        contition.set(stateid!=null,"leave_state",stateid);
        contition.eq("leave_id",leaveId);
        //Leave leave = leaveMapper.selectById(leaveId);*/



        return leaveMapper.updateLeaveUser(leaveId,stateid);
    }
}
