package com.gym.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.gym.dao.UserDeleteMapper;
import com.gym.pojo.entity.UserDelete;
import com.gym.service.UserDeleteService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Service
public class UserDeleteServiceImpl extends ServiceImpl<UserDeleteMapper, UserDelete> implements UserDeleteService {

}
