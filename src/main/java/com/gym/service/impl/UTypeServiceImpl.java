package com.gym.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.gym.dao.UTypeMapper;
import com.gym.pojo.entity.UType;
import com.gym.service.UTypeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Service
public class UTypeServiceImpl extends ServiceImpl<UTypeMapper, UType> implements UTypeService {

}
