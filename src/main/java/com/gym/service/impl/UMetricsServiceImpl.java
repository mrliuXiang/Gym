package com.gym.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.gym.dao.UMetricsMapper;
import com.gym.pojo.entity.UMetrics;
import com.gym.service.UMetricsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Service
public class UMetricsServiceImpl extends ServiceImpl<UMetricsMapper, UMetrics> implements UMetricsService {

}
