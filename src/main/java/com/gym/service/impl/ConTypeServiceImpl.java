package com.gym.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.gym.dao.ConTypeMapper;
import com.gym.pojo.entity.ConType;
import com.gym.service.ConTypeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Service
public class ConTypeServiceImpl extends ServiceImpl<ConTypeMapper, ConType> implements ConTypeService {

}
