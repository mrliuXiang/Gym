package com.gym.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.gym.dao.TypeDetailedMapper;
import com.gym.pojo.entity.TypeDetailed;
import com.gym.service.TypeDetailedService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Service
public class TypeDetailedServiceImpl extends ServiceImpl<TypeDetailedMapper, TypeDetailed> implements TypeDetailedService {

}
