package com.gym.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.gym.dao.EquipmentMapper;
import com.gym.pojo.entity.Equipment;
import com.gym.pojo.entity.User;
import com.gym.service.EquipmentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Service
public class EquipmentServiceImpl extends ServiceImpl<EquipmentMapper, Equipment> implements EquipmentService {

    @Resource
    EquipmentMapper equipmentMapper;

    @Override
    public int addequipm(Equipment equipment) {

        return equipmentMapper.insert(equipment);
    }

    @Override
    public int upequipm(Equipment equipment) {
        UpdateWrapper<Equipment> equip = new UpdateWrapper<>();
        equip.set("equ_state", equipment.getEquState());
        equip.eq("equ_id", equipment.getEquId());
        return equipmentMapper.update(equipment,equip);
    }

    @Override
    public int deleequipm(int equidd) {
        QueryWrapper<Equipment> equdele = new QueryWrapper<>();
        equdele.eq("equ_id",equidd);

        return equipmentMapper.delete(equdele);
    }

    @Override
    public List<Equipment> seleequ() {
        QueryWrapper<Equipment> queryWrapper=new QueryWrapper<>();

        return equipmentMapper.selectList(queryWrapper);
    }
}
