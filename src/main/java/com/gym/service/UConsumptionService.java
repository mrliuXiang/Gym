package com.gym.service;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gym.pojo.entity.UConsumption;
import com.gym.pojo.entity.User;

import java.util.Date;
import java.util.List;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
public interface UConsumptionService extends IService<UConsumption> {

    /**
     * @Author 小飞
     * @描述 查询用户消费记录的方法
     * @Date 2020/7/14 15:22
     * @Param [uconmoney根据消费的金额查询, ucontime根据消费的时间查询]
     * @return java.util.List<com.gym.pojo.entity.UConsumption>
     **/
    List<UConsumption> getConsum(double uconmoney,Date ucontime);

    /**
     * @Author 小飞
     * @描述 用户消费记录的方法
     * @Date 2020/7/14 15:21
     * @Param [uConsumption 用户消费的实体类]
     * @return int
     **/
    int addUcon(UConsumption uConsumption);

    /**
     * 用于查找潜在客户
     * 查看0消费的用户
     * @return List<UConsumption>
     */
    void selectNoMoney();

    /**
     * 学员购买课程
     * @param uConsumption
     * @return int
     *
     */
    int addUConsumption(UConsumption uConsumption);
}
