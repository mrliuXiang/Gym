package com.gym.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gym.pojo.entity.Leave;

import java.util.Date;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
public interface LeaveService extends IService<Leave> {

    /**
     * 按条件分页显示请假列表
     * @param currpage 当前页
     * @param leavestart 开始时间
     * @param leaveend 结束时间
     * @param userid 员工id
     * @param leavestate 请假状态
     * @param username 用户名
     * @return IPage<Leave>
     */
    IPage<Leave> selectLeave(Integer currpage, Date leavestart , Date leaveend , Integer userid , Integer leavestate , String username);

    /**
     * 添加员工请假
     * @param start 开始时间
     * @param end 结束时间
     * @param des 备注(原由)
     * @param state 处理状态
     * @param phone 电话
     * @return int
     */
    int addLeaveUser(Date start , Date end , String des , Integer state, String phone);

    /**
     * 修改员工请假信息
     * @param start
     * @param end
     * @param des
     * @param state
     * @param phone
     * @return int
     */
    //int updateLeaveUser(Leave leave , String phone);

    int updateLeaveUser(Integer userid , Integer leaveid,Date start , Date end , String des , Integer state, String phone);

    /**
     * 处理请假
     * @param leaveId
     * @return
     */
    int updateLeaveState(Integer leaveId , Integer stateid);
}
