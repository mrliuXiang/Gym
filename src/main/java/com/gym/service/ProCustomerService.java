package com.gym.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gym.pojo.entity.ProCustomer;

import java.util.List;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-20
 */
public interface ProCustomerService extends IService<ProCustomer> {

    /**
     * 分页显示
     * 获取潜在客户列表
     * @return List<ProCustomer>
     */
    IPage<ProCustomer> selectProCustomer();

    /**
     * 添加潜在用户
     * @param proCustomer
     * @return
     */
    int insert(ProCustomer proCustomer);

    /**
     * 修改潜在客户的信息
     * @param proCustomer
     * @return int
     */
    int modifyProCustomer(ProCustomer proCustomer);

    /**
     * 获取潜在客户对象
     * @param userid
     * @return
     */
    ProCustomer selectProCustomerOne(Long userid);

    Boolean updaProCustomer(ProCustomer proCustomer);
}
