package com.gym.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gym.pojo.entity.Curriculum_type;
import org.springframework.stereotype.Service;

import java.util.List;


public interface CurriculumTypeService extends IService<Curriculum_type> {

    List<Curriculum_type> getCurriculumTypeName();
}
