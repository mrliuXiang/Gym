package com.gym.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gym.pojo.entity.UserDelete;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
public interface UserDeleteService extends IService<UserDelete> {

}
