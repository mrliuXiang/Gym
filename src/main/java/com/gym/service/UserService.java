package com.gym.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gym.pojo.entity.User;

import java.util.List;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
public interface UserService extends IService<User> {
    /**
     *根据用户编号查找详情
     * @param id 编号
     */
    User getUserByid(Integer id);

    IPage<User> getUserByid(int currpage,String username,String phone,Integer sex);
    /**
     *查询所有用户数据
     *
     */
    IPage<User> getUSerList(int currpage);
    /**
     * 用户登录
     * @param username 用户名
     * @param password 密码
     * @return user
     */
    User userLogin(String username , String password);

    /**
     * 用户修改信息
     * @param userid 用户id
     * @param phone 电话
     * @param username 用户名
     * @param sex 性别
     * @param age 年龄
     * @return boolean
     */
    Boolean modifyUser(String userid , String phone , String username , Long sex , Long age);

    /**
     * 用户密码修改
     * @param userid 用户id
     * @param password 用户密码(旧密码)
     * @param repass 确认密码
     * @return
     */
    Boolean modifyUserPwd(String userid , String password , String repass);

    /**
     * 用户删除
     * @param userid 用户删除
     * @return boolean
     */
    Boolean delUser(String userid);

    /**
     * 新增用户

     * @return int
     */
    int insertUser(User user);

    /**
     * 分页用户信息显示
     * @param start
     * @param expire
     * @param gender
     * @param username
     * @param userid
     * @return IPage<User>
     */
    IPage<User> getPageUser(String start , String expire , Long gender , String username , String userid);


    /**
     * 查看课程学员
     * @param currpage 当前页码
     * @return IPage<User>
     */
    IPage<User> getPageByCon(Integer currpage);

}
