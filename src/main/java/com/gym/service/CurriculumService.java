package com.gym.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gym.pojo.entity.Curriculum;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.Map;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
@Service
public interface CurriculumService extends IService<Curriculum> {
    /**
     * @Author 小飞
     * @描述
     * @Date 2020/7/13 9:18
     * @Param [current与number是分页的第一个是第几页第二个参数是多少行, id是排课的教练的id]
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     **/
    Page<Map<String,Object>> selectListPage(int current, int number, String id);

    /**
     * 查看课程分页
     * @param currpage 当前页
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @param userName 用户名
     * @param cId 课程id
     * @return IPage<Curriculum></>
     */
    IPage<Curriculum> getCurriculumPage(int currpage,Date startTime , Date endTime , String userName , Integer cId);


    IPage<Curriculum> getCurriculumPage(int currpage);
    /**
     * 添加课程
     * @param curriculum
     * @return
     */
    int addCurriculum(Curriculum curriculum);

    /**
     * 删除课程
     * @param currId 课程ID
     * @return int
     */
    int delCurriculum(Integer currId);

    /**
     * 修改课程信息
     * @param curriculum
     * @return int
     *
     */
    int modifyCurriculum(Curriculum curriculum);
}
