package com.gym.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gym.pojo.entity.USurplus;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FireFlyZzz
 * @since 2020-07-09
 */
public interface USurplusService extends IService<USurplus> {

    /**
     * 会员信息查询
     * @param start 开始时间
     * @param expire 结束时间
     * @param gender 性别
     * @param username 用户名
     * @param userid 用户id
     * @return 分页返回信息
     */
    /*查询会员信息*/
    IPage<USurplus> getSuperUser(String start , String expire , Long gender , String username , String userid);

    Integer uprenew(int id, int Increasedtime);

    /**
     * 获取过期的用户
     */
    void overDueUser();
}
