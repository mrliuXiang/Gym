package com.gym;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/*
 * @author lpx
 * @date 2020/7/6 - 10:28
 */
@SpringBootApplication
public class GymApplication {
    public static void main(String[] args) {
        SpringApplication.run(GymApplication.class,args);
    }

}
