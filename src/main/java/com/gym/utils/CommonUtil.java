package com.gym.utils;

import com.gym.service.ProCustomerService;
import com.gym.service.UConsumptionService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: Lpx
 * @Date: 2020/07/20
 * @Time: 20:40
 * @Description:
 */
@Component
public class CommonUtil {
    @Resource
    private ProCustomerService proCustomerService;
    @Resource
    UConsumptionService consumptionService;

    // 声明对象
    public static CommonUtil commonUtil;

    @PostConstruct // 初始化
    public void init() {
        commonUtil = this;

    }

}