/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 8.0.19 : Database - bodybuilding
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bodybuilding` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `bodybuilding`;

/*Table structure for table `c_detailed` */

DROP TABLE IF EXISTS `c_detailed`;

CREATE TABLE `c_detailed` (
  `c_deta_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `curr_id` int NOT NULL,
  `c_deta_state` int NOT NULL,
  PRIMARY KEY (`c_deta_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `c_detailed` */

insert  into `c_detailed`(`c_deta_id`,`user_id`,`curr_id`,`c_deta_state`) values (1,1011,1,0),(2,1012,1,0);

/*Table structure for table `con_type` */

DROP TABLE IF EXISTS `con_type`;

CREATE TABLE `con_type` (
  `con_type_id` int NOT NULL AUTO_INCREMENT,
  `con_type_name` varchar(20) NOT NULL,
  PRIMARY KEY (`con_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `con_type` */

insert  into `con_type`(`con_type_id`,`con_type_name`) values (1,'买课'),(2,'年卡'),(3,'月卡'),(4,'季卡'),(5,'次卡');

/*Table structure for table `curriculum` */

DROP TABLE IF EXISTS `curriculum`;

CREATE TABLE `curriculum` (
  `curr_id` int NOT NULL AUTO_INCREMENT COMMENT '课程ID',
  `curr_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '课程名称',
  `user_id` int NOT NULL COMMENT '用户id(教练)',
  `curr_duration` double NOT NULL COMMENT '课程时长',
  `curr_start` date NOT NULL COMMENT '课程开始时间',
  `type_deta_id` int NOT NULL COMMENT '课程类别',
  `c_deta_id` int NOT NULL COMMENT '课程详情ID',
  `curr_state` int NOT NULL COMMENT '课程状态',
  `curr_end` date NOT NULL COMMENT '课程结束时间',
  `place_id` int NOT NULL COMMENT '课程地点',
  `curr_money` double NOT NULL COMMENT '课程价格',
  PRIMARY KEY (`curr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `curriculum` */

insert  into `curriculum`(`curr_id`,`curr_name`,`user_id`,`curr_duration`,`curr_start`,`type_deta_id`,`c_deta_id`,`curr_state`,`curr_end`,`place_id`,`curr_money`) values (1,'demoData',1002,1,'2020-07-21',1,1,1,'2020-07-22',1,1);

/*Table structure for table `equipment` */

DROP TABLE IF EXISTS `equipment`;

CREATE TABLE `equipment` (
  `equ_id` int NOT NULL AUTO_INCREMENT,
  `equ_name` varchar(50) NOT NULL,
  `equ_state` int NOT NULL,
  `equ_remarks` varbinary(500) DEFAULT NULL,
  PRIMARY KEY (`equ_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

/*Data for the table `equipment` */

insert  into `equipment`(`equ_id`,`equ_name`,`equ_state`,`equ_remarks`) values (1,'跑步机',1,NULL),(2,'跑步机',1,NULL),(3,'跑步机',1,NULL),(4,'跑步机',1,NULL),(5,'跑步机',0,NULL),(6,'跑步机',1,NULL),(7,'跑步机',1,NULL),(8,'跑步机',0,NULL),(9,'跑步机',1,NULL),(10,'跑步机',1,NULL),(11,'健美车',1,NULL),(12,'健美车',1,NULL),(13,'健美车',1,NULL),(14,'健美车',1,NULL),(15,'健美车',0,NULL),(16,'健美车',1,NULL),(17,'健美车',1,NULL),(18,'健美车',1,NULL),(19,'健美车',1,NULL),(20,'健美车',1,NULL),(21,'坐姿推胸器',1,NULL),(22,'坐姿推胸器',1,NULL),(23,'蝴蝶机',1,NULL),(24,'蝴蝶机',1,NULL),(25,'肩部推举器',0,NULL),(26,'肩部推举器',1,NULL),(27,'高拉背训练器',1,NULL),(28,'高拉背训练器',1,NULL),(29,'坐式屈腿训练器',1,NULL),(30,'坐式屈腿训练器',1,NULL),(31,'腹肌训练器',1,NULL),(32,'腹肌训练器',1,NULL),(33,'腹肌训练器',1,NULL),(34,'史密斯训练架',1,NULL),(35,'史密斯训练架',0,NULL),(36,'史密斯训练架',1,NULL),(37,'蹬腿机',1,NULL),(38,'蹬腿机',1,NULL),(39,'蹬腿机',1,NULL),(40,'蹬腿机',1,NULL),(41,'哈克深蹲机',1,NULL),(42,'哈克深蹲机',1,NULL),(43,'T型划船杆',1,NULL),(44,'T型划船杆',1,NULL),(45,'坐姿侧推机',0,NULL),(46,'坐姿侧推机',1,NULL),(47,'坐姿飞鸟机',1,NULL),(48,'坐姿飞鸟机',1,NULL),(49,'双杆练习器',1,NULL),(50,'双杆练习器',1,NULL);

/*Table structure for table `leave` */

DROP TABLE IF EXISTS `leave`;

CREATE TABLE `leave` (
  `leave_id` int NOT NULL,
  `user_id` int NOT NULL,
  `leave_start` date NOT NULL,
  `leave_end` date NOT NULL,
  `leave_remarks` varchar(255) DEFAULT NULL,
  `leave_state` int NOT NULL,
  PRIMARY KEY (`leave_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `leave` */

/*Table structure for table `place` */

DROP TABLE IF EXISTS `place`;

CREATE TABLE `place` (
  `place_id` int NOT NULL,
  `place_name` varchar(50) NOT NULL,
  PRIMARY KEY (`place_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `place` */

/*Table structure for table `pro_customer` */

DROP TABLE IF EXISTS `pro_customer`;

CREATE TABLE `pro_customer` (
  `pro_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint NOT NULL COMMENT '用户Id',
  `last_time` date DEFAULT NULL COMMENT '最后一次联系',
  `pro_talked_type` int DEFAULT NULL COMMENT '联系方式(1:短信,2:微信,3:电话,4:见面',
  `last_talked` int DEFAULT NULL COMMENT '备注:(1:接受,2:犹豫,3:拒绝)',
  `pro_result` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '结果(内容)',
  PRIMARY KEY (`pro_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

/*Data for the table `pro_customer` */

insert  into `pro_customer`(`pro_id`,`user_id`,`last_time`,`pro_talked_type`,`last_talked`,`pro_result`) values (40,1004,NULL,NULL,NULL,NULL),(41,1002,NULL,1,1,'demoData');

/*Table structure for table `type_detailed` */

DROP TABLE IF EXISTS `type_detailed`;

CREATE TABLE `type_detailed` (
  `type_deta_id` int NOT NULL,
  `type_deta_name` varchar(20) NOT NULL,
  PRIMARY KEY (`type_deta_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `type_detailed` */

/*Table structure for table `u_consumption` */

DROP TABLE IF EXISTS `u_consumption`;

CREATE TABLE `u_consumption` (
  `u_con_id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` int NOT NULL COMMENT '用户编号',
  `u_con_money` double NOT NULL COMMENT '消费金额',
  `u_con_time` date DEFAULT NULL COMMENT '消费时间',
  `u_con_remarks` varbinary(500) DEFAULT NULL COMMENT '备注',
  `u_con_type` int NOT NULL COMMENT '消费卡类型',
  PRIMARY KEY (`u_con_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1206 DEFAULT CHARSET=utf8;

/*Data for the table `u_consumption` */

insert  into `u_consumption`(`u_con_id`,`user_id`,`u_con_money`,`u_con_time`,`u_con_remarks`,`u_con_type`) values (1201,1001,500,'2020-07-01','sadsad',2),(1202,1002,100,'2020-07-01','萨达',3),(1203,1003,300,'2020-06-02','djkhjkl',4),(1204,1004,0,NULL,'123123',0),(1205,1005,100,'2020-07-21','demoData',1);

/*Table structure for table `u_metrics` */

DROP TABLE IF EXISTS `u_metrics`;

CREATE TABLE `u_metrics` (
  `u_met_id` int NOT NULL,
  `u_met_weight` double DEFAULT NULL,
  `u_met_bust` double DEFAULT NULL,
  `u_met_waist` double DEFAULT NULL,
  `u_met_hipline` double DEFAULT NULL,
  `u_met_height` double DEFAULT NULL,
  `u_met_bmi` double DEFAULT NULL,
  `u_met_heart` double DEFAULT NULL,
  `u_met_dheart` double DEFAULT NULL,
  `u_met_dthigh` double DEFAULT NULL,
  `u_met_xthigh` double DEFAULT NULL,
  `u_met_arm` double DEFAULT NULL,
  `u_met_fat` double DEFAULT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`u_met_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `u_metrics` */

/*Table structure for table `u_surplus` */

DROP TABLE IF EXISTS `u_surplus`;

CREATE TABLE `u_surplus` (
  `u_surp_id` int NOT NULL,
  `user_id` int NOT NULL,
  `u_surp_expire` date NOT NULL,
  `u_surp_frequency` int NOT NULL,
  `u_surp_start` date NOT NULL,
  PRIMARY KEY (`u_surp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `u_surplus` */

insert  into `u_surplus`(`u_surp_id`,`user_id`,`u_surp_expire`,`u_surp_frequency`,`u_surp_start`) values (1101,1001,'2020-07-30',20,'2020-06-30'),(1102,1002,'2020-07-14',0,'2020-06-02'),(1103,1003,'2020-07-22',0,'2020-06-07');

/*Table structure for table `u_type` */

DROP TABLE IF EXISTS `u_type`;

CREATE TABLE `u_type` (
  `u_type_id` int NOT NULL AUTO_INCREMENT,
  `u_type_name` varchar(20) NOT NULL,
  PRIMARY KEY (`u_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `u_type` */

insert  into `u_type`(`u_type_id`,`u_type_name`) values (1,'经理'),(2,'教练'),(3,'会员'),(4,'员工');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` bigint NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `user_gender` int NOT NULL,
  `user_age` int NOT NULL,
  `user_time` date NOT NULL,
  `user_password` varchar(50) NOT NULL,
  `user_number` varchar(50) NOT NULL,
  `u_type_id` int NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`user_id`,`user_name`,`user_gender`,`user_age`,`user_time`,`user_password`,`user_number`,`u_type_id`) values (1001,'小明',1,33,'2020-07-01','123456','12345678911',1),(1002,'小红',2,34,'2020-07-13','123456','12345678977',3),(1003,'小白',1,25,'2020-07-18','123456','12345678988',2),(1004,'小黄',1,43,'2020-07-20','123456','12345678999',0);

/*Table structure for table `user_delete` */

DROP TABLE IF EXISTS `user_delete`;

CREATE TABLE `user_delete` (
  `u_delete_id` int NOT NULL,
  `u_delete_name` varchar(20) NOT NULL,
  `u_delete_gender` int NOT NULL,
  `u_delete_age` int NOT NULL,
  `u_delete_time` date NOT NULL,
  `u_delete_password` varchar(50) NOT NULL,
  `u_delete_telephone` varchar(50) NOT NULL,
  `u_delete_category` int NOT NULL,
  PRIMARY KEY (`u_delete_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user_delete` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
